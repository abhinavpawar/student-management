package com.assignment3.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.assignment3.activity.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by click on 7/6/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "studentsManager";

    // Contacts table name
    private static final String TABLE_STUDENT = "students";

    // Contacts Table Columns names
    private static final String KEY_ROLL = "roll";
    private static final String KEY_NAME = "name";
    private static final String KEY_IMAGE_PATH = "image_path";
//    static ArrayList<Student> demoList;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_STUDENTS_TABLE = "CREATE TABLE " + TABLE_STUDENT + "("
                + KEY_ROLL + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_IMAGE_PATH + " TEXT" + ")";
        db.execSQL(CREATE_STUDENTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public Boolean addStudent(Student student) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ROLL, student.getStudentRoll());
        values.put(KEY_NAME, student.getStudentName());
        values.put(KEY_IMAGE_PATH, student.getAvatar());

        // Inserting Row
        db.insert(TABLE_STUDENT, null, values);
        db.close(); // Closing database connection
        return true;
    }

    // Getting single contact
    public Student getStudent(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_STUDENT, new String[]{KEY_ROLL,
                        KEY_NAME, KEY_IMAGE_PATH}, KEY_ROLL + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Student student = new Student(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return student;
    }

    // Getting All Contacts
    public ArrayList<Student> getAllStudents(ArrayList<Student> list) {
        // ArrayList<Student> studentList = new ArrayList<Student>();
        // Select All Query

        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        list.clear();        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Student student = new Student();
                student.setStudentRoll(Integer.parseInt(cursor.getString(0)));
                student.setStudentName(cursor.getString(1));
                student.setAvatar(cursor.getString(2));
                // Adding contact to list
                list.add(student);
            } while (cursor.moveToNext());
        }

        // return student list
        return list;
    }

    // Updating single contact
    public Boolean updateStudent(Student student, Integer roll, String name, String imagePath) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_IMAGE_PATH, imagePath);
        values.put(KEY_ROLL, roll);
        // updating row
        db.update(TABLE_STUDENT, values, KEY_ROLL + " = ?",
                new String[]{String.valueOf(student.getStudentRoll())});
        return true;

    }

    // Deleting single contact
    public Boolean deleteStudent(Student student) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_STUDENT, KEY_ROLL + " = ?",
                new String[]{String.valueOf(student.getStudentRoll())});
        db.close();
        return true;
    }

    public void deleteAll(ArrayList<Student> students, DatabaseHandler db) {
        SQLiteDatabase database = db.getWritableDatabase();
        database.delete(TABLE_STUDENT, null, null);
        // database.execSQL("delete * from " + TABLE_STUDENT);
        students.clear();

    }
}

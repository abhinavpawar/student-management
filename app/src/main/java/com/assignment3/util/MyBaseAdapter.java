package com.assignment3.util;

import com.assignment3.activity.Student;

import java.lang.Object;

import android.graphics.BitmapFactory;
import android.widget.*;
import android.view.*;
import android.content.Context;

import java.util.*;

import com.assignment3.activity.R;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class MyBaseAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Student> mStudent;

    public MyBaseAdapter(Context context, ArrayList<Student> student) {
        mInflater = LayoutInflater.from(context);
        mStudent = student;
    }

    @Override
    public int getCount() {
        return mStudent.size();
    }

    @Override
    public Student getItem(int position) {

        return mStudent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.activity_listview, parent, false);
            holder = new ViewHolder();
            holder.avatar = (ImageView) view.findViewById(R.id.icon);
            holder.name = (TextView) view.findViewById(R.id.labelName);
            holder.roll = (TextView) view.findViewById(R.id.labelRoll);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Student student = getItem(position);
        String temp = student.getAvatar();
        if (temp == null) {
            holder.avatar.setImageResource(R.mipmap.elementary_school);
        } else {

            Bitmap tempImage = Bitmap.createBitmap(BitmapFactory.decodeFile(temp));
            float originalHeight = tempImage.getHeight();
            float originalWidth = tempImage.getWidth();
            Float tempNewHeight = originalHeight / originalWidth * 80;
            int newHeight = tempNewHeight.intValue();
            Bitmap resized = Bitmap.createScaledBitmap(tempImage, 80, newHeight, true);
            holder.avatar.setImageBitmap(resized);
        }


        holder.name.setText(student.getStudentName());

        holder.roll.setText(student.getStudentRoll().toString());

        return view;
    }

    private class ViewHolder {
        public ImageView avatar;
        public TextView name;
        public TextView roll;
    }


}


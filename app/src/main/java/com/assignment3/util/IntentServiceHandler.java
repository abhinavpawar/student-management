package com.assignment3.util;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import com.assignment3.activity.MainActivity;
import com.assignment3.activity.Student;

/**
 * Created by click on 7/9/15.
 */
public class IntentServiceHandler extends IntentService {
    final String ADD_STUDENT = "add";
    final String EDIT_STUDENT = "edit";
    final String OPERATION = "operation";
    final String INTENT_ACTION = "mainActivityIntentService";
    final String DELETE_STUDENT = "delete";
    DatabaseHandler db = new DatabaseHandler(this);

    public IntentServiceHandler() {
        super("IntentServiceHandler");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String operation = intent.getExtras().getString(OPERATION);

        switch (operation) {
            case ADD_STUDENT: {
                Integer roll = (Integer) intent.getExtras().get("roll");
                String name = intent.getExtras().getString("name");
                String picturePath = intent.getExtras().getString("picturePath");
                Student student = new Student(roll, name, picturePath);
                if (db.addStudent(student)) {
                    String action = "add";
                    notifyAdapterMain(action, intent);
                }
                break;
            }
            case EDIT_STUDENT: {
                Integer roll = (Integer) intent.getExtras().get("roll");
                String name = intent.getExtras().getString("name");
                String picturePath = intent.getExtras().getString("picturePath");
                Integer rollNoOld = (Integer) intent.getExtras().get("rollNoOld");
                if (db.updateStudent(db.getStudent(rollNoOld), roll, name, picturePath)) {
                    String action = "edit";
                    notifyAdapterMain(action, intent);
                }
                break;
            }
            case DELETE_STUDENT: {
                if (db.deleteStudent(db.getStudent((Integer) intent.getExtras().get("roll")))) {
                    String action = "delete";
                    notifyAdapterMain(action, intent);
                }
                break;
            }


        }
        // Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

    }

    public void notifyAdapterMain(String action, Intent intent) {

        Intent intentForReceiver = new Intent();
        intentForReceiver.setAction(INTENT_ACTION);
        intentForReceiver.putExtra("adapterAction", action);
        if (action.equals("add")) {
            intentForReceiver.putExtras(intent);
        } else if (action.equals("edit")) {
            intentForReceiver.putExtras(intent);
        } else if (action.equals("delete")) {
            intentForReceiver.putExtras(intent);
        }
        sendBroadcast(intentForReceiver);

    }

}


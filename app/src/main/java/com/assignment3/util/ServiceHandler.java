package com.assignment3.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import com.assignment3.activity.MainActivity;
import com.assignment3.activity.Student;

/**
 * Created by click on 7/7/15.
 */
public class ServiceHandler extends Service {
    final String ADD_STUDENT = "add";
    final String EDIT_STUDENT = "edit";

    final String INTENT_ACTION = "mainActivityRedirect";
    final String DELETE_STUDENT = "delete";
    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        String operation = intent.getExtras().getString("operation");
        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
        switch (operation) {
            case ADD_STUDENT: {
                Integer roll = (Integer) intent.getExtras().get("roll");
                String name = intent.getExtras().getString("name");
                String picturePath = intent.getExtras().getString("picturePath");
                Student student = new Student(roll, name, picturePath);
                if (db.addStudent(student)) {
                    String action = "add";
                    notifyAdapterMain(action, intent);
                }
                break;
            }
            case EDIT_STUDENT: {
                Integer roll = (Integer) intent.getExtras().get("roll");
                String name = intent.getExtras().getString("name");
                String picturePath = intent.getExtras().getString("picturePath");
                Integer rollNoOld = (Integer) intent.getExtras().get("rollNoOld");
                if (db.updateStudent(db.getStudent(rollNoOld), roll, name, picturePath)) {
                    String action = "edit";
                    notifyAdapterMain(action, intent);
                }
                break;
            }
            case DELETE_STUDENT: {
                if (db.deleteStudent(db.getStudent((Integer) intent.getExtras().get("roll")))) {
                    String action = "delete";
                    notifyAdapterMain(action, intent);
                }
                break;
            }

            default:

        }
        // Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //broadcastIntent();
    }

    public void notifyAdapterMain(String action, Intent intent) {

        Intent intentForReceiver = new Intent();
        intentForReceiver.setAction(INTENT_ACTION);
        intentForReceiver.putExtra("adapterAction", action);
        if (action.equals("add")) {
            intentForReceiver.putExtras(intent);
        } else if (action.equals("edit")) {
            intentForReceiver.putExtras(intent);
        } else if (action.equals("delete")) {
            intentForReceiver.putExtras(intent);
        }

        sendBroadcast(intentForReceiver);

    }


}

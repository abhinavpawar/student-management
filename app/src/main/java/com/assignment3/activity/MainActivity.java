package com.assignment3.activity;

import com.assignment3.util.*;

import java.util.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.widget.BaseAdapter;
import android.view.Menu;
import android.view.MenuItem;

import com.assignment3.activity.*;
import com.assignment3.util.MyBaseAdapter;

import android.widget.AdapterView.OnItemSelectedListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.view.View;

public class MainActivity extends Activity implements OnItemSelectedListener {
    ArrayList<Student> students = new ArrayList<Student>();
    //ArrayAdapter<Student> adapter;
    MyBaseAdapter adapter;
    Student student;
    Boolean servicesFlag = false;
    Boolean asyncFlag = false;
    Boolean intentServiceFlag = false;
    ListView listView;
    GridView gridView;
    DatabaseHandler db;
    CustomReceiver customReceiver;
    final int VIEW = 1;
    final int EDIT = 2;
    final int DELETE = 3;
    final int DELETE_ALL = 4;
    final String CONTEXTMENU_TITLE = "Select Action";
    final String CONTEXT_ITEM_1 = "View";
    final String CONTEXT_ITEM_2 = "Edit";
    final String CONTEXT_ITEM_3 = "Delete";
    final String CONTEXT_ITEM_4 = "Delete All";
    final String SPINNER_ITEM_1 = "Sort";
    final String SPINNER_ITEM_2 = "By Name";
    final String SPINNER_ITEM_3 = "By Roll No.";
    final String BACKGROUND_MENU_TITLE = "Select Background method";
    final String BACKGROUND_MENU_ITEM_1 = "Async Task";
    final String BACKGROUND_MENU_ITEM_2 = "Services";
    final String BACKGROUND_MENU_ITEM_3 = "Service Intents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //  Button button = (Button) findViewById(R.id.addStudent);

        db = new DatabaseHandler(this);
        setContentView(R.layout.activity_main);
        BackgroundOptionChooser();
        //adapter = new ArrayAdapter<Student>(this, R.layout.activity_listview, R.id.label, students);
        adapter = new MyBaseAdapter(this, students);
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add(SPINNER_ITEM_1);
        categories.add(SPINNER_ITEM_2);
        categories.add(SPINNER_ITEM_3);


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        listView = (ListView) findViewById(R.id.studentListView);


        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        gridView = (GridView) findViewById(R.id.studentGridView);
        gridView.setAdapter(adapter);
        registerForContextMenu(gridView);
        gridView.setVisibility(View.INVISIBLE);
        //students = db.getAllStudents();
        // adapter.notifyDataSetChanged();
        new AsyncPopulateAll().execute();
        customReceiver = new CustomReceiver();
        registerReceiver(customReceiver, new IntentFilter("mainActivityRedirect"));
        registerReceiver(customReceiver, new IntentFilter("mainActivityIntentService"));
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(CONTEXTMENU_TITLE);
        menu.add(0, 1, 1, CONTEXT_ITEM_1);
        menu.add(0, 2, 2, CONTEXT_ITEM_2);
        menu.add(0, 3, 3, CONTEXT_ITEM_3);
        menu.add(0, 4, 4, CONTEXT_ITEM_4);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // return super.onContextItemSelected(item);
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        switch (id) {
            case DELETE_ALL: {
                new AsyncDeleteAll().execute();
                return true;

            }
            case DELETE: {
                //students.remove(adapter.getItem(menuInfo.position));
                // adapter.notifyDataSetChanged();
                //finish();
                if (asyncFlag) {
                    new AsyncDeleteStudent().execute(adapter.getItem(menuInfo.position));
                } else if (servicesFlag) {
                    Intent deleteServiceIntent = new Intent(this, ServiceHandler.class);
                    deleteServiceIntent.putExtra("operation", "delete");
                    deleteServiceIntent.putExtra("roll", adapter.getItem(menuInfo.position).getStudentRoll());
                    startService(deleteServiceIntent);
                } else if (intentServiceFlag) {
                    Intent deleteServiceIntent = new Intent(this, IntentServiceHandler.class);
                    deleteServiceIntent.putExtra("operation", "delete");
                    deleteServiceIntent.putExtra("roll", adapter.getItem(menuInfo.position).getStudentRoll());
                    startService(deleteServiceIntent);
                }
                return true;
                //break;

            }
            case EDIT: {
                Intent intent = new Intent(this, EditStudent.class);
                intent.putExtra("asyncFlag", asyncFlag);
                intent.putExtra("servicesFlag", servicesFlag);
                intent.putExtra("intentServiceFlag", intentServiceFlag);
                intent.putExtra("roll", adapter.getItem(menuInfo.position).roll);
                intent.putExtra("name", adapter.getItem(menuInfo.position).name);
                intent.putExtra("pos", menuInfo.position);
                intent.putExtra("picturePath", adapter.getItem(menuInfo.position).picturePath);
                startActivityForResult(intent, 2);

                return true;
                // break;
            }

            case VIEW: {

                Intent intent = new Intent(this, ViewStudent.class);
                intent.putExtra("roll", adapter.getItem(menuInfo.position).roll);
                //intent.putExtra("name", adapter.getItem(menuInfo.position).name);
                //intent.putExtra("picturePath", adapter.getItem(menuInfo.position).picturePath);
                startActivity(intent);


            }

            default:
                return true;

        }
    }

    public void addStudent(View v) {
        Intent intent = new Intent(this, AddStudent.class);
        intent.putExtra("asyncFlag", asyncFlag);
        intent.putExtra("servicesFlag", servicesFlag);
        intent.putExtra("intentServiceFlag", intentServiceFlag);
        //  students.add("abhinav");
        // adapter.notifyDataSetChanged();
        startActivityForResult(intent, 1);
        // students.add("abhinav");
        //v.refreshDrawableState();
        //  adapter.notifyDataSetChanged();}
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == 1) {
            // Bundle b;

            // b=in.getExtras();
            try {
                Integer roll = (Integer) intent.getExtras().get("roll");
                String name = intent.getExtras().getString("name");
                String picturePath = intent.getExtras().getString("picturePath");
                student = new Student(roll, name, picturePath);
                students.add(student);
                Toast.makeText(getApplicationContext(), "Successfully added", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();

            } catch (Exception ex) {
            }
        } else if (resultCode == 2) {
            Integer pos = (Integer) intent.getExtras().get("pos");
            Integer roll = (Integer) intent.getExtras().get("roll");
            Integer rollOld = (Integer) intent.getExtras().get("rollNoOld");

            String name = intent.getExtras().getString("name");
            String picturePath = intent.getExtras().getString("picturePath");
            adapter.getItem(pos).roll = roll;
            adapter.getItem(pos).picturePath = picturePath;
            adapter.getItem(pos).name = name;
            adapter.notifyDataSetChanged();
            new AsyncEditStudent().execute(intent);
        }
    }

    public void changeViewButton(View v) {
        Button button = (Button) findViewById(R.id.viewToggle);

        if (button.getText().toString().equals("Grid")) {

            gridView.setAdapter(adapter);
            gridView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.INVISIBLE);
            button.setText("List");
        } else if (button.getText().toString().equals("List")) {
            gridView.setVisibility(View.INVISIBLE);
            listView.setVisibility(View.VISIBLE);
            button.setText("Grid");
        }


    }

    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        switch (item) {
            case SPINNER_ITEM_2:
                sortByName();
                break;

            case SPINNER_ITEM_3:
                sortByRoll();
                break;


        }
        // Showing selected spinner item

        //Toast.makeText(getApplicationContext(),item, Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }

    private static Comparator<Student> byName() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return o1.name.compareToIgnoreCase(o2.name);
            }
        };
    }

    public void sortByName() {
        Collections.sort(students, byName());
        adapter.notifyDataSetChanged();
        //  setContentView(R.layout.activity_main);
    }

    private static Comparator<Student> byRoll() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return o1.roll.compareTo(o2.roll);
            }
        };
    }

    public void sortByRoll() {
        Collections.sort(students, byRoll());
        adapter.notifyDataSetChanged();
        //  setContentView(R.layout.activity_main);
    }

    public void BackgroundOptionChooser() {
        final CharSequence[] items = {BACKGROUND_MENU_ITEM_1, BACKGROUND_MENU_ITEM_2,
                BACKGROUND_MENU_ITEM_3};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(BACKGROUND_MENU_TITLE);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(BACKGROUND_MENU_ITEM_1)) {
                    asyncFlag = true;

                } else if (items[item].equals(BACKGROUND_MENU_ITEM_2)) {
                    servicesFlag = true;

                } else if (items[item].equals(BACKGROUND_MENU_ITEM_3)) {
                    intentServiceFlag = true;
                }
            }
        });
        builder.show();
    }

    public void tempOnClick(View v) {
        Intent intent = new Intent();
        intent.setAction("mainActivityRedirect");
        sendBroadcast(intent);
        //  students = db.getAllStudents(students);
        // adapter.notifyDataSetChanged();
    }

    private class AsyncDeleteAll extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            //Toast.makeText(MainActivity.this, "Async started", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            db.deleteAll(students, db);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            // setResult(1,new Intent(AddStudent.class,MainActivity.class));
            Toast.makeText(getApplicationContext(), "Deleted All", Toast.LENGTH_SHORT).show();
        }
    }

    private class AsyncPopulateAll extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // Toast.makeText(MainActivity.this, "Async started", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            students = db.getAllStudents(students);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Retrieved All", Toast.LENGTH_SHORT).show();
        }
    }

    private class AsyncEditStudent extends AsyncTask<Intent, Void, Void> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(MainActivity.this, "Async started", Toast.LENGTH_SHORT).show();
        }

        @Override

        protected Void doInBackground(Intent... intent) {
            Integer rollOld = (Integer) intent[0].getExtras().get("rollNoOld");
            Integer roll = (Integer) intent[0].getExtras().get("roll");
            String name = intent[0].getExtras().getString("name");
            String picturePath = intent[0].getExtras().getString("picturePath");

            db.updateStudent(db.getStudent(rollOld), roll, name, picturePath);
            return null;

        }
    }

    private class AsyncDeleteStudent extends AsyncTask<Student, Void, Student> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(MainActivity.this, "Async started", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Student doInBackground(Student... student) {
            db.deleteStudent(student[0]);
            return student[0];

        }

        @Override
        protected void onPostExecute(Student student) {
            super.onPostExecute(student);
            students.remove(student);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
        }
    }

    public class CustomReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("mainActivityRedirect") || intent.getAction().equals("mainActivityIntentService")) {
                String actionString = intent.getExtras().getString("adapterAction");
                switch (actionString) {
                    case "add": {
                        Integer roll = (Integer) intent.getExtras().get("roll");
                        String name = intent.getExtras().getString("name");
                        String picturePath = intent.getExtras().getString("picturePath");
                        students.add(new Student(roll, name, picturePath));
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), "Successfully added", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case "edit": {
                        Integer roll = (Integer) intent.getExtras().get("rollNoOld");
                        Integer rollNew = (Integer) intent.getExtras().get("roll");
                        String name = intent.getExtras().getString("name");
                        String picturePath = intent.getExtras().getString("picturePath");
                        for (Student student : students) {
                            if (roll.intValue() == student.roll.intValue())
                                student.updateStudent(rollNew, name, picturePath);
                        }
                        adapter.notifyDataSetChanged();
                        break;
                    }
                    case "delete": {
                        Student temp = null;
                        Integer roll = (Integer) intent.getExtras().get("roll");
                        for (Student student : students) {
                            if (roll.intValue() == student.roll.intValue()) {
                                temp = student;
                                break;
                            }

                        }
                        students.remove(temp);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
                        break;
                    }


                }
                adapter.notifyDataSetChanged();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(customReceiver);

    }
}
package com.assignment3.activity;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.content.*;
import android.app.Activity;
import android.os.Bundle;
import android.view.*;
import android.view.MenuItem;
import android.widget.*;
import android.net.Uri;
import android.database.Cursor;

import com.assignment3.util.DatabaseHandler;
import com.assignment3.util.IntentServiceHandler;
import com.assignment3.util.ServiceHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddStudent extends Activity {
    Button button;
    ImageView studentImage;
    Integer roll;
    String name;
    public EditText nameEditText;
    public EditText rollNoEditText;
    String picturePath;
    final int SELECT_FILE = 5;
    final String OPERATION = "operation";
    final int REQUEST_CAMERA = 4;
    DatabaseHandler db = new DatabaseHandler(this);
    Boolean servicesFlag;
    Boolean asyncFlag;
    Boolean intentServiceFlag;
    final String PICTURE_SELECT_TITLE = "Add Photo!";
    final String PICTURE_SELECT_OPTION_1 = "Take Photo";
    final String PICTURE_SELECT_OPTION_2 = "Choose from Library";
    final String PICTURE_SELECT_OPTION_3 = "Cancel";

    //Intent addIntent=new Intent(this,MainActivity.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        studentImage = (ImageView) findViewById(R.id.studentImage);
        studentImage.setImageResource(R.mipmap.elementary_school);
        Intent intent = getIntent();
        asyncFlag = (Boolean) intent.getExtras().get("asyncFlag");
        servicesFlag = (Boolean) intent.getExtras().get("servicesFlag");
        intentServiceFlag = (Boolean) intent.getExtras().get("intentServiceFlag");
    }


    public void addOnClick(View v) {
        //  Intent intent = new Intent(this, MainActivity.class);
        rollNoEditText = (EditText) findViewById(R.id.rollNoEditText);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        // try {
        roll = Integer.parseInt(rollNoEditText.getText().toString());
        name = nameEditText.getText().toString();


        if (asyncFlag) {
            new AsyncAddHandler().execute();
        } else if (servicesFlag) {
            Intent addServiceIntent = new Intent(this, ServiceHandler.class);
            addServiceIntent.putExtra(OPERATION, "add");
            addServiceIntent.putExtra("roll", roll);
            addServiceIntent.putExtra("name", name);
            addServiceIntent.putExtra("picturePath", picturePath);
            startService(addServiceIntent);
            finish();
        } else if (intentServiceFlag) {
            Intent addServiceIntent = new Intent(this, IntentServiceHandler.class);
            addServiceIntent.putExtra(OPERATION, "add");
            addServiceIntent.putExtra("roll", roll);
            addServiceIntent.putExtra("name", name);
            addServiceIntent.putExtra("picturePath", picturePath);
            startService(addServiceIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            studentImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            // String picturePath contains the path of selected Image
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = destination.getAbsolutePath();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("picturePath", picturePath);
                studentImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                // ivImage.setImageBitmap(thumbnail);

            }

        }


    }

    public void addImageOnClick(View v) {
        final CharSequence[] items = {PICTURE_SELECT_OPTION_1, PICTURE_SELECT_OPTION_2,
                PICTURE_SELECT_OPTION_3};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(PICTURE_SELECT_TITLE);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(PICTURE_SELECT_OPTION_1)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(PICTURE_SELECT_OPTION_2)) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, SELECT_FILE);
                } else if (items[item].equals(PICTURE_SELECT_OPTION_3)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private class AsyncAddHandler extends AsyncTask<Void, Void, Student> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(AddStudent.this, "Async started", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Student doInBackground(Void... params) {
            Student tempStudent = new Student(roll, name, picturePath);
            db.addStudent(tempStudent);
            return tempStudent;
        }

        @Override
        protected void onPostExecute(Student student) {
            super.onPostExecute(student);
            redirectMainActivity();
            // setResult(1,new Intent(AddStudent.class,MainActivity.class));
            //    Toast.makeText(getApplicationContext(), "Successfully added", Toast.LENGTH_SHORT).show();
        }
    }

    public void redirectMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("roll", roll);
        intent.putExtra("name", name);
        intent.putExtra("picturePath", picturePath);
        setResult(1, intent);
        finish();
    }


}
package com.assignment3.activity;

import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.*;
import android.app.Activity;

import com.assignment3.util.DatabaseHandler;
import com.assignment3.util.ServiceHandler;

public class ViewStudent extends Activity {
    //Drawable drawable=(Drawable)findViewById;
    Integer rollno;
    Student student;
    DatabaseHandler db;
    TextView roll;
    TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this);
        setContentView(R.layout.activity_view_student);
        Intent intent = getIntent();
        roll = (TextView) findViewById(R.id.rollView);
        name = (TextView) findViewById(R.id.nameView);
        rollno = (Integer) intent.getExtras().get("roll");
        new AsyncFetchData().execute(rollno);


        // String nameStudent = intent.getExtras().getString("name");
        //  String picturePath = intent.getExtras().getString("picturePath");

        //    studentIcon.setImageDrawable();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AsyncFetchData extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... id) {
            student = db.getStudent(id[0]);
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String nameStudent = student.getStudentName();

            String picturePath = student.getAvatar();
            roll.setText(rollno.toString());
            name.setText(nameStudent);
            ImageView studentIcon = (ImageView) findViewById(R.id.studentIcon);
            if (picturePath == null) {
                studentIcon.setImageResource(R.mipmap.elementary_school);
            } else {
                Bitmap bmImg = BitmapFactory.decodeFile(picturePath);
                studentIcon.setImageBitmap(bmImg);
            }

        }
    }

}

package com.assignment3.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;


public class Student {
    Integer roll;
    String name;
    String picturePath;

    public Student() {
    }

    public Student(Integer roll, String name, String picturePath) {
        this.name = name;
        this.roll = roll;
        this.picturePath = picturePath;
    }

    public void setStudentRoll(Integer roll) {
        this.roll = roll;
    }

    public Integer getStudentRoll() {
        return this.roll;
    }

    public String getAvatar() {
        return (this.picturePath);
    }

    public void setAvatar(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getStudentName() {
        return this.name;
    }

    public void setStudentName(String name) {
        this.name = name;
    }

    public void updateStudent(Integer roll, String name, String picturePath) {
        this.name = name;
        this.roll = roll;
        this.picturePath = picturePath;
    }

}

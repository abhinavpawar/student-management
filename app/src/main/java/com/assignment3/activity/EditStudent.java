package com.assignment3.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;
import android.widget.ImageView;
import android.app.*;

import com.assignment3.util.IntentServiceHandler;
import com.assignment3.util.ServiceHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class EditStudent extends Activity {
    EditText roll;
    EditText name;
    ImageView image;
    String picturePath;
    Integer pos;
    Integer rollnoOld;
    final int SELECT_FILE = 5;
    final int REQUEST_CAMERA = 4;
    Boolean servicesFlag;
    Boolean asyncFlag;
    Boolean intentServiceFlag;
    final String PICTURE_SELECT_TITLE = "Add Photo!";
    final String PICTURE_SELECT_OPTION_1 = "Take Photo";
    final String PICTURE_SELECT_OPTION_2 = "Choose from Library";
    final String PICTURE_SELECT_OPTION_3 = "Cancel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_student);

        roll = (EditText) findViewById(R.id.rollNoEditText);
        name = (EditText) findViewById(R.id.nameEditText);
        image = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();

        rollnoOld = (Integer) intent.getExtras().get("roll");
        String nameStudent = intent.getExtras().getString("name");
        picturePath = intent.getExtras().getString("picturePath");
        pos = (Integer) intent.getExtras().get("pos");
        roll.setText(rollnoOld.toString());
        name.setText(nameStudent);
        if (picturePath == null) {
            image.setImageResource(R.mipmap.elementary_school);
        } else {
            Bitmap bmImg = BitmapFactory.decodeFile(picturePath);
            image.setImageBitmap(bmImg);
        }
        //image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        asyncFlag = (Boolean) intent.getExtras().get("asyncFlag");
        servicesFlag = (Boolean) intent.getExtras().get("servicesFlag");
        intentServiceFlag = (Boolean) intent.getExtras().get("intentServiceFlag");


    }

    public void addOnClick(View v) {

        Integer rollString = Integer.parseInt(roll.getText().toString());
        String nameString = name.getText().toString();


        if (asyncFlag) {
            Intent editAsyncIntent = new Intent(this, MainActivity.class);
            editAsyncIntent.putExtra("rollNoOld", rollnoOld);
            editAsyncIntent.putExtra("roll", rollString);
            editAsyncIntent.putExtra("name", nameString);
            editAsyncIntent.putExtra("pos", pos);
            editAsyncIntent.putExtra("picturePath", picturePath);
            setResult(2, editAsyncIntent);
            finish();
        } else if (servicesFlag) {
            Intent editServiceIntent = new Intent(this, ServiceHandler.class);
            editServiceIntent.putExtra("rollNoOld", rollnoOld);
            editServiceIntent.putExtra("roll", rollString);
            editServiceIntent.putExtra("name", nameString);
            editServiceIntent.putExtra("operation", "edit");
            editServiceIntent.putExtra("picturePath", picturePath);
            startService(editServiceIntent);
            finish();
        } else if (intentServiceFlag) {
            Intent editServiceIntent = new Intent(this, IntentServiceHandler.class);
            editServiceIntent.putExtra("rollNoOld", rollnoOld);
            editServiceIntent.putExtra("roll", rollString);
            editServiceIntent.putExtra("name", nameString);
            editServiceIntent.putExtra("operation", "edit");
            editServiceIntent.putExtra("picturePath", picturePath);
            startService(editServiceIntent);
            finish();
        }


    }

    public void imageChange(View v) {
        final CharSequence[] items = {PICTURE_SELECT_OPTION_1, PICTURE_SELECT_OPTION_2,
                PICTURE_SELECT_OPTION_3};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(PICTURE_SELECT_TITLE);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(PICTURE_SELECT_OPTION_1)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(PICTURE_SELECT_OPTION_2)) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, SELECT_FILE);
                } else if (items[item].equals(PICTURE_SELECT_OPTION_3)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            // String picturePath contains the path of selected Image
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = destination.getAbsolutePath();
                image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                //Intent intent=new Intent(this,MainActivity.class);
                // intent.putExtra("picturePath", picturePath);
                // ivImage.setImageBitmap(thumbnail);

            }
        }

    }
}
